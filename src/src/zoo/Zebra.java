package zoo;


public class Zebra extends Animal {

    Zebra(String name) {
        this.name = name;
        this.animalType = animalType.MAMMAL;
    }

    @Override
    void sayName() {
        System.out.println("I'm a " + animalType + ". " + "My name " + name + ".");

    }
}
