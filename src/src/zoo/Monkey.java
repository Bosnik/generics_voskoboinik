package zoo;


public class Monkey extends Animal {
    Monkey(String name) {
        this.name = name;
        this.animalType = animalType.MAMMAL;
    }

    @Override
    void sayName() {
        System.out.println("I'm a " + animalType + ". " + "My name " + name + ".");

    }
}
