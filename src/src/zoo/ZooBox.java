package zoo;

public class ZooBox<A extends Animal> {


    private A animals;

    public void lockAnimal(A animal) {
        this.animals = animal;
    }

    public A getAnimal() {
        return animals;
    }


}
