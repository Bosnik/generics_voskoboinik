package zoo;


public class Shark extends Animal {

    Shark(String name){
        this.name = name;
        this.animalType = animalType.FISH;
    }



    @Override
    void sayName() {
        System.out.println("I'm a "+ animalType +". " + "My name " + name +".");

    }
}
