package Sport;

import java.util.ArrayList;

public class Run {
    public static void main(String[] args) {

        Football football = new Football();
        Hockey hockey = new Hockey();
        Formula formula = new Formula();

        Ball ball = new Ball();
        Car car = new Car();
        Fans fans = new Fans();
        Ice ice = new Ice();

        ArrayList<Constituent> constituentsBall = new ArrayList<>();
        constituentsBall.add(ball);
        ArrayList<Constituent> constituentsFans = new ArrayList<>();
        constituentsFans.add(fans);
        ArrayList<Constituent> constituentsIce = new ArrayList<>();
        constituentsIce.add(ice);
        ArrayList<Constituent> constituentsCar = new ArrayList<>();
        constituentsCar.add(car);


        Sport<Football, Constituent> footballBallSport = new Sport<>(football, constituentsBall);
        Sport<Football, Constituent> footballFansSport = new Sport<>(football, constituentsFans);
        Sport<Hockey, Constituent> hockeyIceSport = new Sport<>(hockey, constituentsIce);
        Sport<Hockey, Constituent> hockeyFansSport = new Sport<>(hockey, constituentsFans);
        Sport<Formula, Constituent> formulaCarSport = new Sport<>(formula, constituentsCar);
        Sport<Formula, Constituent> formulaFansSport = new Sport<>(formula, constituentsFans);

        footballBallSport.cantLiveWithout();
        footballFansSport.cantLiveWithout();
        hockeyIceSport.cantLiveWithout();
        hockeyFansSport.cantLiveWithout();
        formulaCarSport.cantLiveWithout();
        formulaFansSport.cantLiveWithout();

        ArrayList<Constituent> footballConstituents = new ArrayList<>();
        footballConstituents.add(fans);
        footballConstituents.add(ball);
        ArrayList<Constituent> hockeyConstituents = new ArrayList<>();
        hockeyConstituents.add(fans);
        hockeyConstituents.add(ice);
        ArrayList<Constituent> formulaConstituents = new ArrayList<>();
        formulaConstituents.add(fans);
        formulaConstituents.add(car);

        Sport<Football, Constituent> footballPassion = new Sport<>(football, footballConstituents);
        footballPassion.cantLiveWithout();
        Sport<Football, Constituent> hockeyPassion = new Sport<>(football, hockeyConstituents);
        hockeyPassion.cantLiveWithout();
        Sport<Football, Constituent> formulaPassion = new Sport<>(football, formulaConstituents);
        formulaPassion.cantLiveWithout();
    }
}
